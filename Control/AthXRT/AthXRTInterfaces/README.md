# Interfaces for XRT

This package is the basic abstract interfaces used in
the management of XRT in AthXRT device management service.

This structure have been replicated from the AthCUDA service
which have served as inspiration for AthXRT, but may not
be in fact required. All this is currently prototype code 
intended to evaluate the benefit of FPGA accelerators in 
Athena and is not intended for merging in Athena in current state.

See AthXRTServices and AthExXRT README.md files for more 
information.